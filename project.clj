(defproject ohua-server "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.apache.commons/commons-math3 "3.2"]]
  :main ^:skip-aot ohua-server.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}}
  
  :source-paths ["src" "src/clojure"]
  :java-source-paths ["src/java"]
  :test-paths ["test" "test/clojure"]
  
  ; require for safety analysis of Ohua
  :javac-options ["-target" "1.8" "-source" "1.8" "-Xlint:-options" "-g"]
  
  ; configuration for different JVM options
  :jvm-opts ["-Xmx1g" 
             "-XX:+UseConcMarkSweepGC"]
  
  ; Ohua libraries
  :resource-paths ["resources/lib/ohua-0.1.jar" "resources/lib/ohua-lang-0.1.jar"]
)
