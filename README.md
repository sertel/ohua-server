# ohua-server

This is an example project that shows how one can create a simple yet scalable web server with Ohua.

## Installation

This project has the following dependencies:

* Java 1.8
* Clojure 1.6
* Leiningen (http://leiningen.org/)
* Ohua (already present)

It is enough to install Leiningen with the 4 little steps listed here: http://leiningen.org/. Leiningen will download and install Clojure automatically.

## Usage
On the command line navigate to the root folder of this project and issue:

    $ lein run -m ohua-server.core/start 8080
    
to start the web server on port 8080.

## Options

### Server versions

Various variants of the server are possible to be executed.

    $ lein run -m ohua-server.core/run-implicit 8080
    
   executes the implicit pipeline.
    
    $ lein run -m ohua-server.core/run-explicit 8080
    
   executes the explicit pipeline version.

The server can be shut down by killing the Java process (Ctrl+C) on the command line.

NIO versions of the server that make use of the sendfile primitive for zero-copying files to the NIC can be executed via

    $ lein run -m ohua-server.core/run-implicit-nio 8080
    $ lein run -m ohua-server.core/run-explicit-nio 8080
    
### Section mappings

Different section mappings are defined in

    test/clojure/ohua_server/configure.clj
    
Predefined section mappings are

* __acpt-req__ -> "accept" function is put on a separate section that the rest of the functions
* __func/sec__ -> each function is places onto its own section
* __sgl-rd-rp__ -> "accept", "read" and "reply" are mapped to their own sections while the rest is placed on a single section
* __io-func/sec__ -> each io-func is placed one a separate section. "parse" is mapped to the section of "read" while "compose" assigned to the section of "reply"

You can select a specific section mapping via the command line

    $ lein run -m ohua-server.core/run-implicit-nio 8080 acpt-req
    
The default strategy is func/sec.

## Experiments

In order to start an experiment with one or more client threads navigate to the root folder of the project and issue:

    $ lein run -m ohua-server.core-test/client 

The experimental parameters can be easily adjusted in 
    
    test/clojure/ohua_server/core.clj

## License

Copyright © 2014

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
