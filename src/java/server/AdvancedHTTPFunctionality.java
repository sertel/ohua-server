package server;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import server.BasicServerFunctionality.ClassicFileLoader;

import com.ohua.lang.Function;

public abstract class AdvancedHTTPFunctionality {
  
  public static class FileStore extends ClassicFileLoader {
    @Function
    public Object[] store(String resource, String content) throws IOException {
      Object[] old = super.load(resource);
      FileWriter writer = new FileWriter(resource);
      writer.write(content);
      writer.close();
      return old;
    }
  }
  
  public static class ContentExtraction {
    @Function
    public Object[] parseContent(List<String> request) {
      for(String line : request)
        if(line.startsWith("Content:")) return new Object[] { line.substring("Content:".length()).trim() };
      return new Object[] { "" }; // no content present, so create an empty file
    }
  }
  
}
