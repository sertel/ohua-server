package server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.channels.FileChannel;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ohua.lang.Function;

public abstract class BasicServerFunctionality {
  
  /**
   * Accept an incoming request.<br>
   * (Note: This is not the typical way to deal with IO handles.)
   */
  public static class Acceptor {
    private ServerSocketChannel _sc = null;
    
    @Function
    public Object[] accept(int port) {
      try {
        if(_sc == null) _sc = initServerSocket(port);
        return new Object[] { _sc.accept().socket() };
      }
      catch(IOException e) {
        throw new RuntimeException(e);
      }
    }
    
    private ServerSocketChannel initServerSocket(int port) throws IOException {
      ServerSocketChannel sc = ServerSocketChannel.open();
      ServerSocket ss = sc.socket();
      ss.setReceiveBufferSize(1024);
      ss.setReuseAddress(true);
      ss.bind(new InetSocketAddress(port), 20000);
      // sc.configureBlocking(true); -> is the default
      return sc;
    }
  }
  
  /**
   * Read the data from the socket. An empty line marks the end of the message.
   */
  public static class Reader {
    @Function
    public Object[] read(Socket cnn) {
      try {
        BufferedReader in = new BufferedReader(new InputStreamReader(cnn.getInputStream(), "UTF-8"));
        List<String> request = new ArrayList<String>();
        String line = null;
        // for simplicity an empty line marks the end of the request message
        while(!(line = in.readLine()).equals(""))
          request.add(line);
        return new Object[] { cnn,
                             request };
      }
      catch(IOException e) {
        throw new RuntimeException(e);
      }
    }
  }
  
  /**
   * Parse the received message. Assumption: this is an HTTP message.
   */
  public static class HTTPParser {
    private Pattern _pattern = Pattern.compile("(GET|PUT)\\s([\\.\\/a-zA-Z_0-9]+)\\s(HTTP/1.[0|1])");
    
    @Function
    public Object[] parse(List<String> request) {
      String firstLine = request.get(0);
      firstLine.trim();
      Matcher matcher = _pattern.matcher(firstLine);
      matcher.find();
      String requestType = matcher.group(1);
      String resource = matcher.group(2);
      String protocolVersion = matcher.group(3);
      return new Object[] { requestType,
                           resource,
                           protocolVersion };
    }
  }
  
  /**
   * The naive way to ship a file: load it from disk into memory.
   */
  public static class ClassicFileLoader {
    @Function
    public Object[] load(String resource) throws IOException {
      File f = new File(resource);
      if(f.exists()) {
        FileReader r = new FileReader(resource);
        BufferedReader reader = new BufferedReader(r);
        StringBuffer content = new StringBuffer();
        String line = null;
        while((line = reader.readLine()) != null)
          content.append(line);
        reader.close();
        r.close();
        String c = content.toString();
        return new Object[] { c,
                             (long) c.length() * 2 };
      } else {
        return new Object[] { "",
                             0 };
      }
    }
  }
  
  /**
   * Instead of going from Disk to RAM and then to the NIC, this loader only loads a reference
   * that is used by a later function to transfer the file data directly from disk to the NIC
   * (using sendfile primitive).
   */
  public static class NIOFileLoader {
    @Function
    public Object[] loadNio(String resource) {
      try {
        @SuppressWarnings("resource") FileChannel fc = new FileInputStream(resource).getChannel();
        return new Object[] { fc,
                             fc.size() };
      }
      catch(Exception e) {
        e.printStackTrace();
        throw new RuntimeException(e);
      }
    }
  }
  
  /**
   * "Writes" (as in constructs) the HTTP header for the response
   */
  public static class HTTPResponseHeader {
    @Function
    public Object[] compose(long bodyLength) {
      StringBuffer buffer = new StringBuffer();
      buffer.append("HTTP/1.0 200 OK\n");
      buffer.append("Date:");
      buffer.append(new java.util.Date());
      buffer.append("\n");
      buffer.append("Content-Type: text/html\n");
      buffer.append("Content-Length: ");
      buffer.append(bodyLength);
      buffer.append("\n");
      return new Object[] { buffer.toString() };
    }
  }
  
  /**
   * Finally sends the constructed response to the client and closes the connection.
   */
  public static class Responder {
    @Function
    public Object[] reply(Socket cnn, String header, String body) {
      try {
        OutputStreamWriter writer = new OutputStreamWriter(cnn.getOutputStream(), "UTF-8");
        writer.write(header);
        writer.flush();
        writer.write(body);
        writer.flush();
        writer.close();
        cnn.close();
        return new Object[0];
      }
      catch(IOException e) {
        throw new RuntimeException(e);
      }
    }
  }
  
  /**
   * Makes use of the sendfile primitive to avoid copying the requested file to memory first.
   *
   */
  public static class NIOResponder {
    @Function
    public Object[] replyNio(Socket cnn, String header, FileChannel body, long length) {
      try {
        OutputStreamWriter writer = new OutputStreamWriter(cnn.getOutputStream(), "UTF-8");
        writer.write(header);
        writer.flush();
        SocketChannel sc = cnn.getChannel();
        body.transferTo(0, length, sc);
        body.close();
        writer.close();
        cnn.close();
        return new Object[0];
      }
      catch(IOException e) {
        throw new RuntimeException(e);
      }
    }
  }
}
