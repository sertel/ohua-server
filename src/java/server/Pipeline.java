package server;

import java.io.IOException;
import java.net.Socket;
import java.nio.channels.FileChannel;
import java.util.List;

import server.BasicServerFunctionality.ClassicFileLoader;
import server.BasicServerFunctionality.HTTPParser;
import server.BasicServerFunctionality.HTTPResponseHeader;
import server.BasicServerFunctionality.NIOFileLoader;

import com.ohua.lang.Function;

/**
 * Function implementations that handle the actual to formal matching in the functions instead
 * of at the algorithm level. This allows for a simple algorithm pipeline.
 * <p>
 * It also shows that inheritance is well supported to refine functions and their state
 * respectively.
 */
public abstract class Pipeline {
  public static class GETParser extends HTTPParser {
    @Function
    public Object[] parseGet(Socket cnn, List<String> request) {
      Object[] parsed = super.parse(request);
      // dump the protocol version and the request type
      assert parsed[0].equals("GET");
      return new Object[] { cnn,
                           parsed[1] };
    }
  }
  
  public static class PipeFileLoader extends ClassicFileLoader {
    @Function
    public Object[] loadPiped(Socket cnn, String resource) throws IOException {
      Object[] result = super.load(resource);
      return new Object[] { cnn,
                           result[0],
                           result[1] };
    }
  }

  public static class PipeNIOFileLoader extends NIOFileLoader {
    @Function
    public Object[] loadPipedNio(Socket cnn, String resource) {
      Object[] result = super.loadNio(resource);
      return new Object[] { cnn,
                           result[0],
                           result[1] };
    }
  }

  public static class PipeHeaderCreation extends HTTPResponseHeader {
    @Function
    public Object[] composePiped(Socket cnn, String content, long length) {
      return new Object[] { cnn,
                           super.compose(length)[0],
                           content };
    }
  }
  
  public static class NIOPipeHeaderCreation extends HTTPResponseHeader {
    @Function
    public Object[] composePipedNio(Socket cnn, FileChannel content, long length) {
      return new Object[] { cnn,
                           super.compose(length)[0],
                           content,
                           length };
    }
  }
}
