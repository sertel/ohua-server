(ns ohua-server.core
  (:use [com.ohua.compile])
  (:require [ohua-server.configure :all :refer :as conf]))

(ohua :import [server])

(defn run-explicit
  "A very verbose version of the server pipeline that assignes only the args that are really required by the functions."
  ([port] (run-explicit port "func/sec"))
  ([port strategy]
  ; parse the command line arg and convert properly to integer (Clojure only knows Numbers/long)
  (let [typed-port (int (read-string port))]
    (println "Opening web server on port" port "...")
    
    ; the web server algorithm is constructed here
    (ohua 
      (let [[cnn request] (read (accept typed-port))]
        (let [[request-type resource protocol-version] (parse request)]
          (let [[file-content file-length] (load resource)]
            (reply cnn (compose file-length) file-content))))

      ; runtime configuration      
      (:run-with-config (conf/configure strategy))))))


(defn run-explicit-nio
  "A very verbose version of the server pipeline that assignes only the args that are really required by the functions."
  ([port] (run-explicit-nio port "func/sec"))
  ([port strategy]
  ; parse the command line arg and convert properly to integer (Clojure only knows Numbers/long)
  (let [typed-port (int (read-string port))]
    (println "Opening web server on port" port "...")
    
    ; the web server algorithm is constructed here
    (ohua 
      (let [[cnn request] (read (accept typed-port))]
        (let [[request-type resource protocol-version] (parse request)]
          (let [[file-channel file-length] (load-nio resource)]
            (reply-nio cnn (compose file-length) file-channel file-length))))
      
      ; runtime configuration      
      (:run-with-config (conf/configure strategy))))))


(defn run-implicit
  "A straight forward version of the server flow that makes use of the according operators."
  ([port] (run-implicit port "func/sec"))
  ([port strategy]
  ; parse the command line arg and convert properly to integer (Clojure only knows Numbers/long)
  (let [typed-port (int (read-string port))]
    (println "Opening web server on port" port "...")
    
    ; the web server algorithm is constructed here (using Clojure's threading macro)
    (ohua 
      (-> typed-port accept read parse-get load-piped compose-piped reply)
      
      ; runtime configuration      
      (:run-with-config (conf/configure strategy))))))


(defn run-implicit-nio
  "A straight forward version of the server flow that makes use of the according operators."
  ([port] (run-implicit-nio port "func/sec"))
  ([port strategy]
  ; parse the command line arg and convert properly to integer (Clojure only knows Numbers/long)
  (let [typed-port (int (read-string port))]
    (println "Opening web server on port" port "...")
    
    ; the web server algorithm is constructed here (using Clojure's threading macro)
    (ohua 
      (-> typed-port accept read parse-get load-piped-nio compose-piped-nio reply-nio)
      
      ; runtime configuration      
      (:run-with-config (conf/configure strategy))))))

(defn run-get-and-post
  "A web server version that supports HTTP GET and PUT messages."
  [port]
  ; parse the command line arg and convert properly to integer (Clojure only knows Numbers/long)
  (let [typed-port (int (read-string port))]
    (println "Opening web server on port" port "...")
    
    ; the web server algorithm is constructed here
    (ohua 
      (let [[cnn request] (-> typed-port accept read)
            [type resource _] (parse request)]
        (let [[content length] (if (= type "GET")
                                 (load resource)
                                 (store resource (parse-content request)))]
          (reply cnn (compose length) content)))
      
      ; runtime configuration      
      (:run-with-config (conf/configure "sync-resource")))))


(defn start "General entry point." ([] (start "8080")) ([port] (run-explicit port)))