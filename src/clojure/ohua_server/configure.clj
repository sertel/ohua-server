(ns ohua-server.configure)

(defn create-default-concurrency-config
  "Basic runtime parameters such as thread count, resource constraints etc."
  [props]
  (doto props
    ; uncomment to run deterministically in single-threaded mode (for debugging purposes)
    (.setProperty "execution-mode" (.name (com.ohua.engine.RuntimeProcessConfiguration$Parallelism/MULTI_THREADED)))

    ; resource constraints on the application
    (.setProperty "inter-section-arc-boundary" "100")
    (.setProperty "inner-section-arc-boundary" "0")
                    
    ; parallelism degree
    (.setProperty "core-thread-pool-size" "20")
    (.setProperty "max-thread-pool-size" "20")
                    
    ; quantas to guarantee fairness and processing requirements
    (.setProperty "operator-quanta" "100") ; this quanta refers to the actions of a single op
    (.setProperty "scheduling-quanta" "200000") ; this quanta refers to the scheduling operations of the operator scheduler
    (.setProperty "arc-activation" "1") ; prefer latency 
    ))

(defn create-section-mapping
  "Assigns functions to sections."
  [strategy]
  (cond 
    (= strategy "acpt-req") '(["accept.*"] ["read.*" "parse.*" "load.*" "compose.*" "reply.*"])
    (= strategy "func/sec") '(["accept.*"] ["read.*"] ["parse.*"] ["load.*"] ["compose.*"] ["reply.*"])
    (= strategy "io-func/sec") '(["accept.*"] ["read.*" "parse.*"] ["load.*"] ["compose*" "reply.*"])
    (= strategy "sgl-rd-rp") '(["accept.*"] ["read.*"] ["parse.*" "load.*" "compose.*"] ["reply.*"])
    (= strategy "sync-resource") '(["accept.*"] ["read.*"] ["parse.*"] ["if.*"] ["load.*" "store.*"] ["merge.*"] ["compose.*"] ["reply.*"])))

(defn configure
  "Configuration of the Ohua runtime system."
  [strategy]
  (doto (new com.ohua.engine.RuntimeProcessConfiguration) 
    (.setProperties 
      (doto (create-default-concurrency-config (new java.util.Properties))
            (.put "section-strategy" "com.ohua.system.sections.mappers.ConfigurableSectionMapper")
            (.put "section-config" (create-section-mapping strategy))))))