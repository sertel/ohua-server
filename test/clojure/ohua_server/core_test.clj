(ns ohua-server.core-test
  (:require [clojure.test :refer :all]
            [ohua-server.core :refer :all]
            [clojure.java.io :as io]))

(defn client
  "Starts one or a set of clients that continuously request a resource from the server."
  ([] (client "GET"))
  ([type]
  ; prepare some files first
  (map io/delete-file (file-seq (io/file "test/data")))
  (doto (new com.ohua.experiments.web.HttpServerExperimentSetup)
    (.createFiles (doto (java.util.Properties.) 
                    (.putAll {
                              ; server address details
                              "destination" "test/data" ; path to resource directory
                              "file-count" "10"
                              "size" "512" ; size of the files to be retrieved
                              }))))
  ; run the client(s)
  (let [start-time (System/currentTimeMillis)]
    (doto (new com.ohua.experiments.web.HttpWebServerExperiment$Experiment)
      (.execute (doto (java.util.Properties.) 
                  (.putAll {
                            ; server address details
                            "server-address" "localhost"
                            "server-port" "8080"
                            "url" "test/data" ; path to resource directory
                         
                            ; server resource details
                            "max-file-num" "10"
                         
                            ; experiment details
                            "client-count" "2" 
                            "request-delay" "20" ;ms
                            "experiment-length" "10000" ;ms
                            
                            ; client type
                            "request-type" type
                            "content-size" "512" ; for PUT requests
                            })))
        (.generateStatistics start-time)))))